<?php

namespace XCompany\Core\Tests\Common;

use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use XCompany\Core\Common\EmailAddress;

final class EmailAddressTest extends TestCase
{
    public function testCreateEmailAddress(): void
    {
        $address = new EmailAddress('mori@gmail.com');
        $this->assertTrue($address instanceof EmailAddress);

        $this->expectException(InvalidArgumentException::class);
        $badAddress = new EmailAddress('mori');
    }

    public function testGetAddress(): void
    {
        $address = new EmailAddress('Mori@GMail.com');
        $this->assertEquals($address->address(), 'mori@gmail.com');
    }

    public function testGetDomain(): void
    {
        $address = new EmailAddress('Mori@GMail.com');
        $this->assertEquals($address->domain(), 'gmail.com');
    }

    public function testGetIdentifer(): void
    {
        $address = new EmailAddress('Mori@GMail.com');
        $this->assertEquals($address->identifier(), 'mori');
    }
}
