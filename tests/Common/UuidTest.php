<?php

namespace XCompany\Core\Tests\Common;

use PHPUnit\Framework\TestCase;
use XCompany\Core\Common\Uuid;

final class UuidTest extends TestCase
{
    public function testCreateUuid(): void
    {
        $uuid = new Uuid();
        $this->assertTrue($uuid instanceof Uuid);
    }

    public function testCreateFromString(): void
    {
        $fromString = Uuid::fromString('319EE230-9642-4BA6-B1DD-B6F2B0E8817E');
        $this->assertEquals(strtolower('319EE230-9642-4BA6-B1DD-B6F2B0E8817E'), $fromString->toString());

        $fromHex = Uuid::fromString($fromString->toHex());
        $this->assertTrue($fromHex->equalsTo($fromString));

        $fromBinary = Uuid::fromBytes($fromString->toBytes());
        $this->assertTrue($fromBinary->equalsTo($fromHex));
    }

    public function testUuidsAreOrdered(): void
    {
        $ids = array_map(function ($item) {
            return new Uuid();
        }, range(1, 100));

        $last = $ids[0];
        for ($i = 1; $i < 100; $i++) {
            $this->assertTrue((string)$ids[$i] > (string)$last);
            $last = $ids[$i];
        }
    }
}
