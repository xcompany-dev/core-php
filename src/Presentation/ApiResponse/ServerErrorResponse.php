<?php

namespace XCompany\Core\Presentation\ApiResponse;

use Assert\Assertion;
use Illuminate\Http\JsonResponse;

class ServerErrorResponse implements Response, HasJsonResponse
{
    use InteractWithHeaders;

    /**
     * @var string
     */
    private $message;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $meta;

    /**
     * @var array
     */
    private $headers;

    public function __construct(string $message, int $statusCode, array $data, array $headers)
    {
        Assertion::greaterOrEqualThan($statusCode, 500);
        Assertion::lessThan($statusCode, 600);

        $this->message = $message;
        $this->statusCode = $statusCode;
        $this->data = $data;
        $this->meta = [];
        $this->headers = $headers;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function toJsonResponse(): JsonResponse
    {
        return new JsonResponse(
            [
                'message' => $this->message,
            ],
            $this->statusCode,
            $this->headers
        );
    }
}
