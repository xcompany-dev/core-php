<?php

namespace XCompany\Core\Infrastructure\Application;

use Illuminate\Contracts\Container\Container;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use XCompany\Core\Application\GenericQuery;
use XCompany\Core\Application\Query;
use XCompany\Core\Application\QueryHandler;
use XCompany\Core\Application\QueryHandlerLocator;
use XCompany\Core\Application\QueryHandlerNotFoundException;

final class  PsrContainerQueryHandlerLocator implements QueryHandlerLocator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function locate(Query $query): QueryHandler
    {
        try {
            $handlerName = $query instanceof GenericQuery
                ? $query->queryName() . 'Handler'
                : get_class($query) . 'Handler';
            return $this->container->make($handlerName);
        } catch (\Exception $e) {
            if ($e instanceof NotFoundExceptionInterface || $e instanceof ContainerExceptionInterface) {
                throw new QueryHandlerNotFoundException();
            }

            throw $e;
        }
    }
}
