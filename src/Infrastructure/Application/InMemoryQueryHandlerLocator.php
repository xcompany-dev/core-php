<?php

namespace XCompany\Core\Infrastructure\Application;

use XCompany\Core\Application\Query;
use XCompany\Core\Application\QueryHandler;
use XCompany\Core\Application\QueryHandlerLocator;
use XCompany\Core\Application\QueryHandlerNotFoundException;

final class InMemoryQueryHandlerLocator implements QueryHandlerLocator
{
    /**
     * @var array
     */
    private $handlers;

    public function __construct(array $handlers)
    {
        $this->handlers = $handlers;
    }

    public function locate(Query $query): QueryHandler
    {
        if (array_key_exists($query->queryName(), $this->handlers)) {
            return $this->handlers[$query->queryName()];
        }

        throw new QueryHandlerNotFoundException();
    }

    public function addHandler(string $queryName, QueryHandler $handler)
    {
        $this->handlers[$queryName] = $handler;

        return $this;
    }
}
