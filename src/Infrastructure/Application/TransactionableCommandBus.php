<?php

namespace XCompany\Core\Infrastructure\Application;

use XCompany\Core\Application\Command;
use XCompany\Core\Application\CommandBus;
use XCompany\Core\Application\UnitOfWork;
use XCompany\Core\Common\Event;

final class TransactionableCommandBus implements CommandBus
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var UnitOfWork
     */
    private $unitOfWork;

    public function __construct(CommandBus $commandBus, UnitOfWork $unitOfWork)
    {
        $this->commandBus = $commandBus;
        $this->unitOfWork = $unitOfWork;
    }

    public function dispatch(Command $command)
    {
        return $this->unitOfWork->transactional(function () use ($command) {
            $result = $this->commandBus->dispatch($command);
            if ($result) {
                if (is_array($result)) {
                    $this->unitOfWork->recordEvents(array_filter($result, function ($item) {
                        return $item instanceof Event;
                    }));
                }
            }

            return $result;
        });
    }
}
