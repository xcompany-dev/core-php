<?php

namespace XCompany\Core\Infrastructure\Application;

use XCompany\Core\Application\Command;
use XCompany\Core\Application\CommandBus;
use XCompany\Core\Application\CommandHandlerLocator;

final class SimpleCommandBus implements CommandBus
{
    /**
     * @var CommandHandlerLocator
     */
    private $handlerLocator;

    public function __construct(CommandHandlerLocator $handlerLocator)
    {
        $this->handlerLocator = $handlerLocator;
    }

    public function dispatch(Command $command)
    {
        $handler = $this->handlerLocator->locate($command);
        return $handler->handle($command);
    }
}
