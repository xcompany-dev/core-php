<?php

namespace XCompany\Core\Infrastructure\Domain;

trait InteractWithDelayedJobs
{
    protected $delayedJobs = [];

    public static function bootInteractWithDelayedJobs()
    {
        static::saved(function ($model) {
            foreach ($model->getDelayedJobs() as $job) {
                $job($model);
            }
        });
    }

    public function runDelayed(\Closure $job): void
    {
        $this->delayedJobs[] = $job;
    }

    public function getDelayedJobs()
    {
        $jobs = $this->delayedJobs;
        $this->delayedJobs = [];
        return $jobs;
    }
}
