<?php

namespace XCompany\Core\Infrastructure\Container;

interface ContainerProvider
{
    /**
     * Return absolute path of container
     *
     * @return string
     */
    public function getRootPath(): string;

    /**
     * Returns a pair of view path
     * first element is view path
     * second element is namespace of views
     *
     * @return array
     */
    public function getViewsPath(): array;

    /**
     * Returns containers's routes path both api and web
     *
     * @return array
     */
    public function getRoutesPaths(): array;

    /**
     * Returns array of bindings
     * array keys are abstracts
     * array values are implementations
     *
     * @return array
     */
    public function getBindings(): array;

    /**
     * Returns array of events and their related event listener
     *
     * @return array
     */
    public function getEvents(): array;

    /**
     * Returns path of database migrations
     *
     * @return string
     */
    public function getMigrationsPath(): ?string;

    /**
     * Returns a list of registered CLI commands
     *
     * @return array
     */
    public function getConsoleCommands(): array;

    /**
     * Returns Container specific Service Provider list
     * @return array
     */
    public function getServiceProviders(): array;
}
