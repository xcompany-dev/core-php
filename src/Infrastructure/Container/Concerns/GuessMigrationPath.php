<?php

namespace XCompany\Core\Infrastructure\Container\Concerns;

trait GuessMigrationPath
{
    public function getMigrationsPath(): ?string
    {
        return $this->getRootPath() . '/Infrastructure/Migrations';
    }
}
