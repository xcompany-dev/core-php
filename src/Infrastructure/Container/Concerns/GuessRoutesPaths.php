<?php

namespace XCompany\Core\Infrastructure\Container\Concerns;

trait GuessRoutesPaths
{
    public function getRoutesPaths(): array
    {
        return [
            $this->getRootPath() . '/Presentation/Api/Routes.php',
            $this->getRootPath() . '/Presentation/Web/Routes.php',
        ];
    }
}
