<?php

namespace XCompany\Core\Infrastructure;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Fluent;
use Illuminate\Validation\ValidationException;
use XCompany\Core\Application\Command;
use XCompany\Core\Application\CommandValidationResult;
use XCompany\Core\Application\CommandValidator;

abstract class LaravelValidator
{
    /**
     * @var Factory
     */
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function validate(array $input)
    {
        $validation = $this->factory->make(
            $input,
            $this->rules(new Fluent($input))
        );

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
    }

    public function validateRequest(Request $request)
    {
        $this->validate($request->all());
    }

    abstract public function rules(Fluent $input): array;
}
