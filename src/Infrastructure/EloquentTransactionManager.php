<?php

namespace XCompany\Core\Infrastructure;

use Illuminate\Database\Connection;
use XCompany\Core\Application\TransactionManager;

final class EloquentTransactionManager implements TransactionManager
{
    /**
     * @var Connection
     */
    private $db;

    private $level;

    public function __construct(Connection $db)
    {
        $this->db = $db;
        $this->level = 0;
    }

    public function begin(): void
    {
        if ($this->level === 0) {
            $this->db->beginTransaction();
        }

        ++$this->level;
    }

    public function rollback(): void
    {
        $this->db->rollBack();
    }

    public function isZeroLevel(): bool
    {
        return $this->level === 0;
    }

    public function commit(): void
    {
        --$this->level;

        if ($this->level === 0) {
            $this->db->commit();
        }
    }
}
