<?php

namespace XCompany\Core\Infrastructure;

use XCompany\Core\Common\Event;

interface EventListener
{
    public function handle(Event $event): void;
}
