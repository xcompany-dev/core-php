<?php

namespace XCompany\Core\Infrastructure\Migration;

use Illuminate\Database\Schema\Grammars\MySqlGrammar;
use Illuminate\Support\Fluent;

final class MysqlSchemaGrammar extends MySqlGrammar
{
    public function typeBinaryUuid(Fluent $column)
    {
        return "BINARY({$column->length})";
    }
}
