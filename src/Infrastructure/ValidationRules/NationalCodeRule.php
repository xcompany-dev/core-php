<?php

namespace XCompany\Core\Infrastructure\ValidationRules;

use Illuminate\Contracts\Validation\Rule;
use XCompany\Core\Common\NationalCode;

final class NationalCodeRule implements Rule
{
    public function passes($attribute, $value)
    {
        return NationalCode::validate($value);
    }

    public function message()
    {
        return ':attribute is not valid national code';
    }
}
