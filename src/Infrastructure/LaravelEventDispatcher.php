<?php

namespace XCompany\Core\Infrastructure;

use Illuminate\Contracts\Events\Dispatcher;
use XCompany\Core\Application\EventDispatcher;
use XCompany\Core\Common\Event;

final class LaravelEventDispatcher implements EventDispatcher
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function dispatch(Event $event): void
    {
        $this->dispatcher->dispatch($event);
    }
}
