<?php

namespace XCompany\Core\Common;

use Assert\Assertion;

abstract class Enum implements \JsonSerializable
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * @var string
     */
    private $key;

    /**
     * @var array
     */
    private static $constants = [];

    /**
     * Enum constructor.
     * @param $value
     */
    public function __construct($value)
    {
        if (is_numeric($value)) {
            $value = (int)$value;
        }

        if (is_float($value)) {
            $value = (float)$value;
        }

        Assertion::inArray($value, static::toArray());

        $this->value = $value;
        $this->key = array_search($value, static::toArray());
    }

    /**
     * @return array
     */
    public static function values(): array
    {
        return array_values(static::toArray());
    }

    /**
     * @param string $key
     * @return static
     */
    public static function fromKey(string $key)
    {
        return call_user_func([static::class, $key]);
    }

    /**
     * @return array
     */
    public static function keys(): array
    {
        return array_keys(static::toArray());
    }

    /**
     * @return float|int
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function key(): string
    {
        return $this->key;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function toArray(): array
    {
        if (array_key_exists(static::class, self::$constants) === false) {
            $class = new \ReflectionClass(static::class);

            self::$constants[static::class] = $class->getConstants();
        }

        return self::$constants[static::class];
    }

    /**
     * @param $array
     * @return Enum
     */
    public static function __set_state($array)
    {
        return new static($array['value']);
    }

    /**
     * @param $method
     * @param $arguments
     * @return bool|mixed
     */
    public function __call($method, $arguments)
    {
        foreach (static::toArray() as $key => $value) {
            $virtualMethod = 'is' . studly_case(strtolower($key));

            if ($method === $virtualMethod) {
                return $this->value() === $value;
            }
        }

        throw new \BadMethodCallException("No method or enum constant '$method' in class " . get_called_class());
    }

    /**
     * If there is a constant as "PERSON" we can make an instance
     * by calling "Enum::person()"
     *
     * @param $method
     * @param $arguments
     * @return static
     */
    public static function __callStatic($method, $arguments)
    {
        foreach (static::toArray() as $key => $value) {
            $virtualMethod = camel_case(strtolower($key));

            if ($method === $virtualMethod) {
                return new static($value);
            }
        }

        throw new \BadMethodCallException("No static method or enum constant '$method' in class " . get_called_class());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->value();
    }

    /**
     * @param Enum $other
     * @return bool
     */
    public function equalsTo(Enum $other): bool
    {
        return $this->value() === $other->value() && get_class($this) === get_class($other);
    }

    public function jsonSerialize()
    {
        return (string)$this;
    }
}
