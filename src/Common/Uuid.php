<?php

namespace XCompany\Core\Common;

use Ramsey\Uuid\Codec\TimestampFirstCombCodec;
use Ramsey\Uuid\Generator\CombGenerator;
use Ramsey\Uuid\UuidFactory;

/**
 * Class Uuid
 * @package XCompany\Core\Common
 */
final class Uuid implements \JsonSerializable
{
    /**
     * @var Uuid
     */
    private $id;

    /**
     * Uuid constructor.
     * @param null $value
     * @throws \Exception
     */
    public function __construct($value = null)
    {
        $this->id = is_string($value) && !empty($value)
            ? $this->getUuidFactory()->fromString($value)
            : $this->getUuidFactory()->uuid4();
    }

    /**
     * @return UuidFactory
     */
    protected function getUuidFactory()
    {
        $factory = new UuidFactory;

        $factory->setRandomGenerator(new CombGenerator(
            $factory->getRandomGenerator(),
            $factory->getNumberConverter()
        ));

        $factory->setCodec(new TimestampFirstCombCodec(
            $factory->getUuidBuilder()
        ));

        return $factory;
    }

    /**
     * @return Uuid
     * @throws \Exception
     */
    public static function generate(): Uuid
    {
        return new self();
    }

    /**
     * @param $value
     * @return Uuid
     * @throws \Exception
     */
    public static function fromString($value): Uuid
    {
        return new self($value);
    }

    /**
     * @param $value
     * @return Uuid
     * @throws \Exception
     */
    public static function fromHex($value): Uuid
    {
        return new self($value);
    }

    /**
     * @param $value
     * @return Uuid
     * @throws \Exception
     */
    public static function fromBytes($value): Uuid
    {
        return new self(bin2hex($value));
    }

    /**
     * @param Uuid $other
     * @return bool
     */
    public function equalsTo(Uuid $other): bool
    {
        return (string)$other === (string)$this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * @return string
     */
    public function toHex(): string
    {
        return $this->id->getHex();
    }

    /**
     * @return string
     */
    public function toBytes(): string
    {
        return hex2bin($this->toHex());
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->id->toString();
    }

    /**
     * @param string $id
     * @return bool
     */
    public static function isValid(string $id): bool
    {
        return \Ramsey\Uuid\Uuid::isValid($id);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->toString();
    }
}
