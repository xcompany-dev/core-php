<?php

namespace XCompany\Core\Common;

use Assert\Assertion;

final class EmailAddress
{
    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var mixed
     */
    private $domain;

    /**
     * EmailAddress constructor.
     * @param string $address
     */
    public function __construct(string $address)
    {
        Assertion::email($address);

        $this->address = mb_strtolower($address);
        $this->identifier = implode(explode('@', $this->address, -1), '@');
        $this->domain = str_replace($this->identifier . '@', '', $this->address);
    }

    /**
     * @return string
     */
    public function identifier(): string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function domain(): string
    {
        return $this->domain;
    }

    /**
     * @param EmailAddress $other
     * @return bool
     */
    public function equalsTo(EmailAddress $other): bool
    {
        return $this->address() === $other->address();
    }

    /**
     * @return string
     */
    public function address(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->address;
    }
}
