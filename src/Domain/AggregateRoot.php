<?php

namespace XCompany\Core\Domain;

interface AggregateRoot
{
    /**
     * @param DomainEvent $event
     */
    public function recordThat(DomainEvent $event): void;

    /**
     * @return DomainEvent[]
     */
    public function getRecordedEvents(): array;

    /**
     * @return mixed
     */
    public function getId();
}
