<?php

namespace XCompany\Core\Domain;

final class GenericDomainEvent implements DomainEvent
{
    /**
     * @var string
     */
    private $eventName;

    /**
     * @var array
     */
    private $payload;

    public function __construct(string $eventName, array $payload)
    {
        $this->eventName = $eventName;
        $this->payload = $payload;
    }

    public function get(string $key)
    {
        return $this->payload[$key] ?? null;
    }

    public function all(): array
    {
        return $this->payload;
    }

    public function eventName(): string
    {
        return $this->eventName;
    }
}
