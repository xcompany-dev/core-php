<?php

namespace XCompany\Core\Application;

final class QueryHandlerNotFoundException extends \Exception implements ApplicationException
{
}
