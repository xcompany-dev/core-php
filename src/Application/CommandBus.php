<?php

namespace XCompany\Core\Application;

interface CommandBus
{
    public function dispatch(Command $command);
}
