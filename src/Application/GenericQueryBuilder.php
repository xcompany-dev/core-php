<?php

namespace XCompany\Core\Application;

use Assert\Assertion;
use XCompany\Core\Presentation\QueryRequestParser;

final class GenericQueryBuilder
{
    /**
     * @var string
     */
    private $name;

    private $filters;

    private $sorts;

    private $take;

    private $skip;

    private $term;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->filters = [];
        $this->sorts = [];
        $this->take = 1;
        $this->skip = 0;
    }

    public static function create(string $name): GenericQueryBuilder
    {
        return new self($name);
    }

    public static function fromHttpRequestParser(string $queryName, QueryRequestParser $parser): GenericQueryBuilder
    {
        $builder = new static($queryName);

        foreach ($parser->getFilters() as $filter) {
            $builder->filterBy($filter);
        }

        foreach ($parser->getSorts() as $sort) {
            $builder->sortBy($sort);
        }

        $builder->take($parser->getPerPage());
        $builder->skip(($parser->getCurrentPage() - 1) * $parser->getPerPage());
        $builder->withTerm($parser->getTerm());

        return $builder;
    }

    public function filterBy(Filter $filter)
    {
        $this->filters[] = $filter;

        return $this;
    }

    public function sortBy(Sort $sort)
    {
        $this->sorts[] = $sort;

        return $this;
    }

    public function take(int $take)
    {
        $this->take = $take;

        return $this;
    }

    public function skip(int $skip)
    {
        $this->skip = $skip;

        return $this;
    }

    public function withTerm(string $term)
    {
        $this->term = $term;

        return $this;
    }

    public function paginate(int $currentPage, int $perPage)
    {
        Assertion::min($currentPage, 1);

        $this->skip = ($currentPage - 1) * $perPage;
        $this->take = $perPage;

        return $this;
    }

    public function build(): GenericQuery
    {
        return new GenericQuery(
            $this->name,
            $this->term,
            $this->filters,
            $this->sorts,
            $this->take,
            $this->skip
        );
    }
}
