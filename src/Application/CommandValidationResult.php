<?php

namespace XCompany\Core\Application;

final class CommandValidationResult
{
    /**
     * @var array
     */
    private $errors;

    public function __construct(array $errors = [])
    {
        $this->errors = $errors;
    }

    public function fails(): bool
    {
        return empty($this->errors) === false;
    }

    public function errors(): array
    {
        return $this->errors;
    }
}
