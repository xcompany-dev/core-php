<?php

namespace XCompany\Core\Application;

final class Filter implements \JsonSerializable
{
    /**
     * @var string
     */
    private $field;

    /**
     * @var FilterOperator
     */
    private $operator;

    /**
     * @var string|array
     */
    private $value;

    /**
     * Filter constructor.
     * @param string $field
     * @param FilterOperator $operator
     * @param $value
     */
    public function __construct(string $field, FilterOperator $operator, $value)
    {
        if ($operator->isIn() === false && $operator->isNotIn() === false) {
            $this->assertScalar($value);
        }

        $this->field = $field;
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return FilterOperator
     */
    public function getOperator(): FilterOperator
    {
        return $this->operator;
    }

    /**
     * @return array|string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $field
     * @param array $values
     * @return Filter
     */
    public static function in(string $field, array $values): Filter
    {
        return new self($field, FilterOperator::in(), $values);
    }

    /**
     * @param string $field
     * @param $value
     * @return Filter
     */
    public static function equals(string $field, $value): Filter
    {
        return new self($field, FilterOperator::eq(), $value);
    }

    /**
     * @param string $field
     * @param $value
     * @return Filter
     */
    public static function notEquals(string $field, $value): Filter
    {
        return new self($field, FilterOperator::neq(), $value);
    }

    /**
     * @param string $field
     * @param $value
     * @return Filter
     */
    public static function greaterThan(string $field, $value): Filter
    {
        return new self($field, FilterOperator::gt(), $value);
    }

    /**
     * @param string $field
     * @param $value
     * @return Filter
     */
    public static function lessThan(string $field, $value): Filter
    {
        return new self($field, FilterOperator::lt(), $value);
    }

    /**
     * @param string $field
     * @param $value
     * @return Filter
     */
    public static function lessThanOrEqualsTo(string $field, $value): Filter
    {
        return new self($field, FilterOperator::lte(), $value);
    }

    /**
     * @param string $field
     * @param $value
     * @return Filter
     */
    public static function greaterThanOrEqualsTo(string $field, $value): Filter
    {
        return new self($field, FilterOperator::gte(), $value);
    }

    /**
     * @param string $field
     * @param $value
     * @return Filter
     */
    public static function notIn(string $field, $value): Filter
    {
        return new self($field, FilterOperator::nin(), $value);
    }

    /**
     * @param $value
     */
    private function assertScalar($value): void
    {
        if (false === is_scalar($value)) {
            throw new \InvalidArgumentException('Value must be scalar');
        }
    }

    public function __toString()
    {
        return json_encode([
            'field' => $this->field,
            'operator' => $this->operator->getOperator(),
            'value' => (string)$this->value
        ]);
    }

    public function jsonSerialize()
    {
        return (string)$this;
    }
}
