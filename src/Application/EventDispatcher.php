<?php

namespace XCompany\Core\Application;

use XCompany\Core\Common\Event;

interface EventDispatcher
{
    public function dispatch(Event $event): void;
}
