<?php

namespace XCompany\Core\Application\Concerns;

trait HasNotTermQuery
{
    public function term(): ?string
    {
        return null;
    }
}
