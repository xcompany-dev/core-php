<?php


namespace XCompany\Core\Application\Concerns;

trait GuessQueryName
{
    public function queryName(): string
    {
        $base = explode('\\', get_class($this));

        return end($base);
    }
}
