<?php

namespace XCompany\Core\Application\Concerns;

trait IsOkCommandResult
{
    public function errors(): array
    {
        return [];
    }

    public function isOk(): bool
    {
        return true;
    }
}
