<?php

namespace XCompany\Core\Application\Concerns;

trait GuessCommandName
{
    public function commandName(): string
    {
        $base = explode('\\', get_class($this));

        return end($base);
    }
}
