<?php

namespace XCompany\Core\Application;

use XCompany\Core\Common\Event;
use XCompany\Core\Infrastructure\ExceptionEvent;

final class UnitOfWork
{

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    private $events;

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    public function __construct(TransactionManager $transactionManager, EventDispatcher $dispatcher)
    {
        $this->transactionManager = $transactionManager;
        $this->dispatcher = $dispatcher;
        $this->events = [];
    }

    /**
     * @param \Closure $operation
     * @return mixed
     */
    public function transactional(\Closure $operation)
    {
        try {
            $this->transactionManager->begin();
            $result = $operation();
            $this->transactionManager->commit();

            if ($this->transactionManager->isZeroLevel()) {
                while ($event = array_shift($this->events)) {
                    $this->dispatcher->dispatch($event);
                }
            }

            return $result;
        } catch (\Exception $e) {
            $this->transactionManager->rollback();
            $this->dispatcher->dispatch(new ExceptionEvent($e));
            throw $e;
        }
    }

    /**
     * @param array $events
     * @return void
     */
    public function recordEvents(array $events): void
    {
        $this->events = array_merge($this->events, array_filter($events, function ($event) {
            return $event instanceof Event;
        }));
    }
}
