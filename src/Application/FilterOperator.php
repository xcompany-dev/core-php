<?php

namespace XCompany\Core\Application;

/**
 * Class FilterOperator
 * @package XCompany\Core\Application
 */
final class FilterOperator
{
    private const EQ = 1;
    private const GT = 2;
    private const LT = 3;
    private const GTE = 4;
    private const LTE = 5;
    private const NEQ = 6;
    private const IN = 7;
    private const NIN = 8;

    /**
     * @var int
     */
    private $operator;

    /**
     * FilterOperator constructor.
     * @param int $operator
     */
    public function __construct(int $operator)
    {
        if (false === in_array($operator, range(1, 8), true)) {
            throw new \InvalidArgumentException('Invalid operator');
        }

        $this->operator = $operator;
    }

    /**
     * @return bool
     */
    public function isEquals(): bool
    {
        return $this->operator === self::EQ;
    }

    /**
     * @return bool
     */
    public function isGreaterThan(): bool
    {
        return $this->operator === self::GT;
    }

    /**
     * @return bool
     */
    public function isGreaterThanOrEquals(): bool
    {
        return $this->operator === self::GTE;
    }

    /**
     * @return bool
     */
    public function isLessThan(): bool
    {
        return $this->operator === self::LT;
    }

    /**
     * @return bool
     */
    public function isLessThanOrEquals(): bool
    {
        return $this->operator === self::LTE;
    }

    /**
     * @return bool
     */
    public function isIn(): bool
    {
        return $this->operator === self::IN;
    }

    /**
     * @return bool
     */
    public function isNotEquals(): bool
    {
        return $this->operator === self::NEQ;
    }

    /**
     * @return bool
     */
    public function isNotIn(): bool
    {
        return $this->operator === self::NIN;
    }

    /**
     * @param FilterOperator $operator
     * @return bool
     */
    public function equalsTo(FilterOperator $operator): bool
    {
        return $this->getOperator() === $operator->getOperator();
    }

    /**
     * @return int
     */
    public function getOperator(): int
    {
        return $this->operator;
    }

    /**
     * @return FilterOperator
     */
    public static function eq(): FilterOperator
    {
        return new self(self::EQ);
    }

    /**
     * @return FilterOperator
     */
    public static function neq(): FilterOperator
    {
        return new self(self::NEQ);
    }

    /**
     * @return FilterOperator
     */
    public static function gt(): FilterOperator
    {
        return new self(self::GT);
    }

    /**
     * @return FilterOperator
     */
    public static function lt(): FilterOperator
    {
        return new self(self::LT);
    }

    /**
     * @return FilterOperator
     */
    public static function gte(): FilterOperator
    {
        return new self(self::GTE);
    }

    /**
     * @return FilterOperator
     */
    public static function lte(): FilterOperator
    {
        return new self(self::LTE);
    }

    /**
     * @return FilterOperator
     */
    public static function in(): FilterOperator
    {
        return new self(self::IN);
    }

    /**
     * @return FilterOperator
     */
    public static function nin(): FilterOperator
    {
        return new self(self::NIN);
    }
}
