<?php

namespace XCompany\Core\Application;

use XCompany\Core\Common\Option;

final class GenericCommand implements Command
{
    /**
     * @var string
     */
    private $commandName;

    /**
     * @var array
     */
    private $payload;

    public function __construct(string $commandName, array $payload)
    {
        $this->commandName = $commandName;
        $this->payload = $payload;
    }

    public function all(): array
    {
        return $this->payload;
    }

    public function get(string $key)
    {
        return $this->payload[$key] ?? null;
    }

    public function commandName(): string
    {
        return $this->commandName;
    }
}
